# MJA - Mintel Jira Analysis

A Django project that uses the Atlassian Jira REST API and Postgres to perform analysis on Mintel Jira tickets.

## Development

### Setup

Clone into the project and create a `.env` file in the root directory with the following fields:

```
POSTGRES_PASSWORD = <password of your choosing>
POSTGRES_USER = postgres
JIRA_API_TOKEN = <token>
```

To set up the docker image for the project run:

```
make build
```

Then to start the dev environment:

```
make up
```

The project will be accessible from http://localhost:12000

Then run migrations with:

```
./scripts/manage migrate
```

And create a superuser:

```
./scripts/manage createsuperuser
```
