from django.db import models


class Sprint(models.Model):
    sprint_id = models.IntegerField()


class Ticket(models.Model):
    key = models.CharField(unique=True)
    link = models.CharField()
    title = models.CharField()
    last_updated = models.DateTimeField(auto_now=True)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, null=True)


class StatusChange(models.Model):
    from_string = models.CharField()
    to_string = models.CharField()
    jira_id = models.IntegerField()
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    date = models.DateTimeField()


class Status(models.Model):
    label = models.CharField()
    duration = models.DurationField()
    duration_percentage = models.FloatField()
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
