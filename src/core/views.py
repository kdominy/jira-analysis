import json

from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers import serialize
from django.http import Http404, JsonResponse
from django.views.generic import View
from jira import JIRAError

from .models import Sprint, Status, StatusChange, Ticket
from .utils import process_sprint, process_ticket


class TicketView(View):
    def get(self, request, ticket_key, *args, **kwargs):
        try:
            ticket = self._get_ticket(ticket_key)
        except JIRAError:
            raise Http404

        return JsonResponse(
            data={
                "key": ticket.key,
                "link": ticket.link,
                "title": ticket.title,
                "lastUpdated": ticket.last_updated,
                "statusTimes": [
                    json.loads(serialize("json", [status]))[0]["fields"]
                    for status in Status.objects.filter(ticket__key=ticket.key)
                ],
                "statusChanges": [
                    json.loads(serialize("json", [change]))[0]["fields"]
                    for change in StatusChange.objects.filter(
                        ticket__key=ticket.key
                    ).order_by("date")
                ],
            }
        )

    def _get_ticket(self, ticket_key):
        try:
            ticket = Ticket.objects.get(key=ticket_key)
        except ObjectDoesNotExist:
            ticket = process_ticket(ticket_key)

        return ticket


class SprintView(View):
    def get(self, request, sprint_id, *args, **kwargs):
        try:
            tickets = self._get_tickets(sprint_id)
        except JIRAError:
            raise Http404

        data = []
        for ticket in tickets:
            data.append(
                {
                    "key": ticket.key,
                    "link": ticket.link,
                    "title": ticket.title,
                    "statusTimes": [
                        json.loads(serialize("json", [status]))[0]["fields"]
                        for status in Status.objects.filter(
                            ticket__key=ticket.key
                        )
                    ],
                }
            )

        return JsonResponse(data=data, safe=False)

    def _get_tickets(self, sprint_id):
        try:
            Sprint.objects.get(sprint_id=sprint_id)
            tickets = Ticket.objects.filter(sprint__sprint_id=sprint_id)
        except ObjectDoesNotExist:
            tickets = process_sprint(sprint_id)

        return tickets
