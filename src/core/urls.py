from django.urls import include, path

from . import views

api = [
    path(
        "ticket/<slug:ticket_key>/", views.TicketView.as_view(), name="ticket"
    ),
    path("sprint/<str:sprint_id>/", views.SprintView.as_view(), name="sprint"),
]

urls = [path("api/", include(api))]
