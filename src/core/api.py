import os

from jira import JIRA

from .consts import MINTEL_JIRA_URL

jira = JIRA(
    server=MINTEL_JIRA_URL,
    basic_auth=(
        "kdominy@mintel.com",
        os.environ.get("JIRA_API_TOKEN"),
    ),
)
