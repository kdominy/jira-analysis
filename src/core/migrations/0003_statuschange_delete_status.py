# Generated by Django 4.2.2 on 2023-09-08 16:01

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0002_alter_status_ticket_alter_ticket_key"),
    ]

    operations = [
        migrations.CreateModel(
            name="StatusChange",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("from_string", models.CharField()),
                ("to_string", models.CharField()),
                ("jira_id", models.IntegerField()),
                ("date", models.DateField()),
                (
                    "ticket",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="core.ticket",
                    ),
                ),
            ],
        ),
        migrations.DeleteModel(
            name="Status",
        ),
    ]
