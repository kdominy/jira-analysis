from datetime import datetime, timedelta

from .api import jira
from .consts import DATETIME_FORMAT, MINTEL_JIRA_URL
from .models import Sprint, Status, StatusChange, Ticket


def get_sprint_issues(sprint_id):
    jql = f"sprint = {sprint_id}"
    issues = jira.search_issues(jql)

    return issues


def process_ticket(ticket_key):
    issue = jira.issue(ticket_key)
    ticket = Ticket(
        key=issue.key,
        link=f"{MINTEL_JIRA_URL}/browse/{issue.key}",
        title=issue.fields.summary,
        sprint=None,
    )
    ticket.save()
    _add_status_updates(ticket)
    _process_status_changes(ticket)

    return ticket


def process_sprint(sprint_id):
    issues = get_sprint_issues(sprint_id)
    sprint = Sprint(sprint_id=sprint_id)
    sprint.save()
    tickets = []
    for issue in issues:
        ticket = process_ticket(issue.key)
        ticket.sprint = sprint
        ticket.save()
        tickets.append(ticket)

    return tickets


def _add_status_updates(ticket):
    issue = jira.issue(ticket.key, expand=["changelog"])
    for update in issue.changelog.histories:
        existing_changes = StatusChange.objects.filter(jira_id=update.id)
        for item in update.items:
            if item.field == "status" and not existing_changes.exists():
                change = StatusChange(
                    jira_id=update.id,
                    date=datetime.strptime(update.created, DATETIME_FORMAT),
                    from_string=item.fromString,
                    to_string=item.toString,
                    ticket=ticket,
                )
                change.save()
            elif item.field == "status" and existing_changes.exists():
                existing_change = existing_changes.first()
                existing_change.jira_id = update.id
                existing_change.date = datetime.strptime(
                    update.created, DATETIME_FORMAT
                )
                existing_change.from_string = item.fromString
                existing_change.to_string = item.toString
                existing_change.ticket = ticket
                existing_change.save()


def _process_status_changes(ticket):
    changes = StatusChange.objects.filter(ticket__key=ticket.key).order_by(
        "date"
    )

    # Delete exisiting statuses
    status_list = Status.objects.filter(ticket__key=ticket.key)
    for status in status_list:
        status.delete()
    for change in changes:
        if not Status.objects.filter(
            label=change.to_string, ticket__key=ticket.key
        ).exists():
            status = Status(
                label=change.to_string,
                ticket=ticket,
                duration=timedelta(0),
                duration_percentage=0,
            )
            status.save()

    # Add durations
    for idx, change in enumerate(changes):
        if idx > 0:
            status = Status.objects.filter(
                label=change.from_string,
                ticket__key=ticket.key,
            ).first()
            time_delta = change.date - changes[idx - 1].date
            status.duration += time_delta
            status.save()

    # Calculate percentage
    status_list = Status.objects.filter(ticket__key=ticket.key)
    durations = [
        status.duration.total_seconds()
        for status in Status.objects.filter(ticket__key=ticket.key)
    ]
    total = sum(durations)
    for status in status_list:
        status.duration_percentage = round(
            status.duration.total_seconds() / total * 100, 1
        )
        status.save()
