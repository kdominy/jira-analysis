.PHONY: help # Generate list of targets with descriptions
help:
	@grep '^.PHONY: .* #' Makefile | sed 's/\.PHONY: \(.*\) # \(.*\)/\1	\2/' | expand -t20

.PHONY: build # Builds the docker image used for development
build:
	docker compose build

.PHONY: up # Starts the docker-compose development services
up:
	docker compose up

.PHONY: format-be # Formats .py files using isort and black
format-be:
	cd src/ && poetry run isort . && poetry run black .

.PHONY: lint-be # Lints .py files using isort and flake8
lint-be:
	cd src/ && poetry run flake8 && poetry run isort . -c
