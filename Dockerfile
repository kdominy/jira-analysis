FROM python:3.10 AS backend

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="/root/.local/bin:$PATH"
RUN poetry config virtualenvs.create false
COPY . /web-app
WORKDIR /web-app
RUN poetry install --without dev

FROM backend AS development

RUN poetry install
CMD ["poetry", "run", "python", "src/manage.py", "runserver", "0.0.0.0:12000"]
